/* Greeting */
//Before we start, we'll clear the screen for the player so they don't see anything else.
print("\u{001B}[2J")

//Let's greet the player to our amazing mad lib by printing a few messages to the terminal. Because I can't hit ENTER to make a new line, I will use the \n symbol instead.
print(" Ethan's Transforming Mad Lib\nVersion 2.0\n(C) 2017. Licensed under Apache 2.0 License")
print("\n\nWelcome, player! Are you ready for a fun game of Mad Libs? I am! Let's start.\n")
print("Be sure to type your words in all capitals so you can easily identify them later.")

/* Words to Fill in Mad Lib */
//First, let's create some variables that will be used to make our mad lib work! These variables will contain our words for us. Because our mad lib requires words, we'll make variables that hold strings. If we were to assign a variable anything, it would infer its type.
//Let's create the nouns
var firstNoun: String
var secondNoun: String
var thridNoun: String

//verbs
var firstVerb: String
var secondVerb: String
var thirdVerb: String

//And the adjectives
var firstAdjective: String
var secondAdjective: String

//Finally, I'll create some extra variables make it a bit more interesting
var firstName:String


/* Mad Lib Questionnaire */
//Now that we have created some variables, now we need to fill them. Let's ask the player for a name.
print("Please enter a name", terminator: ": ")

//Now, we need to read what the player wrote and store it into the first variable we created.
firstName = readLine()!

//Let's repeat the process for the rest of the nouns.
print("Please enter a noun", terminator: ": ")
firstNoun = readLine()!

print("Please enter another noun", terminator: ": ")
secondNoun = readLine()!

print("Please enter one more noun", terminator: ": ")

//Now, time for the adjectives:
print("Please enter an adjective", terminator: ": ")
firstAdjective = readLine()!

print("Please enter another adjective", terminator: ": ")
secondAdjective = readLine()!

print("Please enter one final adjective", terminator: ": ")

//And the verbs:
print("Please enter a verb with an -ing", terminator: ": ")
firstVerb = readLine()!

print("Please enter another verb", terminator: ": ")
secondVerb = readLine()!

print("Please enter another verb", terminator: ": ")
thirdVerb = readLine()!

print("Please enter one final verb (this is the last verb, we promise)", terminator: ": ")
fourthVerb = readLine()!

//And the adverb:
print("Please enter an adverb", terminator: ": ")
firstAdverb = readLine()!


/* Mad Lib Story */
//Before we continue, we'll clear the screen for the player so they don't see anything else.
print("\u{001B}[2J")

//Finally, it's time to write the story! Remember: we can't put in any of the variables we just made like a word. We have to use the variable name in order for it to work; to make it easy, we do something like this: \(firstAdjective) . I'm going to break it up into several print commands so it's a bit easier to read.
print("In a \(firstAdjective) town called Faketon, lived a \(secondAdjective) boy named, \(firstName)./n")
print("One day, \(firstName) was \(firstVerb) down a hill and at the bottom, saw a \(firstNoun)./n")
print("\(firstName) decided to \(secondVerb) it and he \(firstAdverb) transformed into a(n) \(thirdAdjective) \(secondNoun)./n")
print("\(firstName) \(thridVerb) his new form. After \(firstNumber) years, \(firstName) liked the new look./n")
print("\(firstName) later decided to \(fourthVerb) a bunch of \(thirdNoun)./n")
print("\(firstName) was \(emotion) with his/her new life./n")
print("The End./n")
