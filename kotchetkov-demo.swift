//Before we start, we'll clear the screen for the player so they don't see anything else.
print("\u{001B}[2J")
print("\n\nHello World test!\n")

/* Words to Fill in Mad Lib */
//First, let's create some variables that will be used to make our mad lib work! These variables will contain our words for us. Because our mad lib requires words, we'll make variables that hold strings. 
//If we were to assign a variable anything, it would infer its type.
//Let's create the nouns
var firstNoun: String
var secondNoun: String
var thirdNoun: String

//And the adjectives
var firstAdjective: String
var secondAdjective: String

//And the verbs
var firstVerb: String
var secondVerb: String
var thirdVerb: String
var fourthVerb: String

//And the adverb
var firstAdverb: String

/* Mad Lib Questionnaire */
//Now that we have created some variables, now we need to fill them. Let's ask the player for the first noun.
//First, we need to print a message to the terminal. 
//The user will know when to input something as the terminator will make it appear like this: "Please enter a noun: ".
print("Please enter a noun", terminator: ": ")

//Now, we need to read what the player wrote and store it into the first variable we created.
firstNoun = readLine()!

//Let's repeat the process for the rest of the nouns.
print("Please enter another noun", terminator: ": ")
secondNoun = readLine()!

print("Please enter one final noun", terminator: ": ")
thirdNoun = readLine()!

//Now, time for the adjectives:
print("Please enter an adjective", terminator: ": ")
firstAdjective = readLine()!

print("Please enter another adjective", terminator: ": ")
secondAdjective = readLine()!

//And the verbs:
print("Please enter a verb", terminator: ": ")
firstVerb = readLine()!

print("Please enter another verb", terminator: ": ")
secondVerb = readLine()!

print("Please enter another verb", terminator: ": ")
thirdVerb = readLine()!

print("Please enter one final verb (this is the last verb, we promise)", terminator: ": ")
fourthVerb = readLine()!

//And the adverb:
print("Please enter an adverb", terminator: ": ")
firstAdverb = readLine()!

/* Mad Lib Story */
//Before we continue, we'll clear the screen for the player so they don't see anything else.
print("\u{001B}[2J")

//Finally, it's time to write the story! Remember: we can't put in any of the variables we just made like a word. 
//We have to use the variable name in order for it to work; to make it easy, we do something like this: \(firstAdjective). 
//I'm going to break it up into several print commands so it's a bit easier to read.
print("Good afternoon! Damien Memorial School is located in \(firstNoun), in the City of \(secondNoun) of the State of \(thirdNoun). I \(firstVerb) to the School every morning by bus. The bus \(secondVerb) \(firstAdverb). It is usually \(firstAdjective) when sun rises in the morning. The streets become \(secondAdjective) when more and more people \(fourthVerb) ready for work.\n")
