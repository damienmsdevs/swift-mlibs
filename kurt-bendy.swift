/*
 
 Bendy and the Swift Machine
 (C) 2017 Licensed under Apache 2.0 License: learn more at https://www.apache.org/licenses/LICENSE-2.0
 Version 1.0

 Description: A mysterious place with a mysterious ink demon? Sure, this fan-made mad lib describes it all!
 
 */

//Clear screen (in function form)

//Init variables
var firstNoun: String
var secondNoun: String
var thirdNoun: String
var fourthNoun: String

var firstAdjective: String
var secondAdjective: String

var firstVerb: String
var secondVerb: String
var thirdVerb: String
var fourthVerb: String
var fifthVerb: String

var firstAdverb: String

var firstName: String
var secondName: String
var thirdName: String
var fourthName: String

var firstNumber: String
var secondNumber: String

var songTitle: String

print("\u{001B}[2J")

print("Bendy and the Swift Machine!\nAn amazing mad lib by Marquis Kurt\n(C) 2017. Licensed under Apache License 2.0\nBendy and Bendy and the Ink Machine are registered trademarks of theMeatly Games.")
print("\n\nHey there! Welcome to this amazing mad lib! Are you ready to start? I sure am! Let's do this.")

print("Please enter a noun.", terminator: ": ")
firstNoun = readLine()!
	
print("Another noun, please.", terminator: ": ")
secondNoun = readLine()!
	
print("Another noun, please.", terminator: ": ")
thirdNoun = readLine()!
	
print("Another noun, please.", terminator: ": ")
fourthNoun = readLine()!
	
print("Please enter an adjective.", terminator: ": ")
firstAdjective = readLine()!
	
print("Please enter an adjective.", terminator: ": ")
secondAdjective = readLine()!
	
print("Please enter a verb.", terminator: ": ")
firstVerb = readLine()!
	
print("Please enter another verb.", terminator: ": ")
secondVerb = readLine()!
	
print("Please enter another verb.", terminator: ": ")
thirdVerb = readLine()!
	
print("Please enter another verb.", terminator: ": ")
fourthVerb = readLine()!
	
print("Please enter another verb.", terminator: ": ")
fifthVerb = readLine()!
	
print("Please enter an adverb.", terminator: ": ")
firstAdverb = readLine()!
	
print("Please enter a name.", terminator: ": ")
firstName = readLine()!
	
print("Please enter a name.", terminator: ": ")
secondName = readLine()!
	
print("Please enter a name.", terminator: ": ")
thirdName = readLine()!
	
print("Please enter a name.", terminator: ": ")
fourthName = readLine()!
	
print("Please enter a number.", terminator: ": ")
firstNumber = readLine()!
	
print("Please enter a number.", terminator: ": ")
secondNumber = readLine()!
	
print("Please enter the name of a song.", terminator: ": ")
songTitle = readLine()!

print("\u{001B}[2J")
print("Here goes!\n")
print("In the distant past, a man named \(thirdName) created an animation studio called \(firstAdjective) \(secondNoun) Studios to compete with Disney and Max Fliescher. He worked on cartoons about a \(firstNoun) named \(fourthName) that always \(thirdVerb).\n")
print("He asked Joey Drew about using his \(secondAdjective) Ink Machine to recreate his animations and bring them to life. He hesitated but let him use it anyway. He turned on the ink machine and started to \(secondVerb) while the ink machine did its thing. He could hear \(songTitle) play on the radio as he \(firstAdverb) did this.\n")
print("However, things didn't go according to plan. Bendy found out about this and tried to \(firstVerb) him and all of his \(fourthNoun). He flooded the room with \(thirdNoun) and he, in addition to Joey Drew, drowned in the ink.\n")
print("\nThe year is \(secondNumber). Henry receives a letter from Joey Drew, inviting him to \(fifthVerb) at the old studio. He unwillingly complies and enters the old studio, noticing how old it was. He begins to reminisce about the times he had with \(secondName) and \(fourthName). With that in mind, his \(firstNumber) challenges begin...")
