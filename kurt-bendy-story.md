# Bendy and the Swift Machine

_An Interesting Mad Lib by the Developers' Club_

<img src = "https://www.rafu.com/wp-content/uploads/2014/08/david-ige2.png" align = "right" width = "20%"/>

In the distant past, a man named **Obama** created an animation studio called **Easy Spoon** to compete with Disney and Max Fliescher. He worked on cartoons about a **dinner** named '**David Ige**' that always **solves**.

He asked Joey Drew about using his **strict** Ink Machine to recreate his animations and bring them to life. He hesitated but let him use it anyway. He turned on the ink machine and started to **hit** while the ink machine did its thing. He could hear '**Shake It Off**' play on the radio as he **stupidely&ast;** did this.

<img src = "https://img10.deviantart.net/bcdc/i/2017/127/4/e/bendy_and_the_ink_machine_chapter_1_poster_by_bearbro123-db8h71c.jpg" align = "left" width = "40%"/>

However, things didn't go according to plan. Bendy found out about this and tried to **sprint** him and all of his **brains**. He flooded the room with **president** and he, in addition to Joey Drew, drowned in the ink.

> The year is **23000**. Henry receives a letter from Joey Drew, inviting him to **sit** at the old studio. He unwillingly accepts and enters the old studio, noticing how old it was. He begins to reminisce about the times he had with **Hillary Clinton** and **Donald Trump**. With that in mind, his **61** challenges begin...

![Hil+Don](https://az616578.vo.msecnd.net/files/2016/06/12/636013349259047675857230710_HillaryClinton-DonaldTrump_web.png)

_&ast; This spelling was intentional for comedic effect._