# Mad Libs (The Swift Edition)

_Note: This is not a **library** for Swift, even though the repository name includes 'libs'._

<img src = "https://pbs.twimg.com/profile_images/491944915085955072/ItTCkYa1.jpeg"  width = "25%" align = "right" />

_Mad Libs is a registered trademark of Penguin Random House._

This repository contains multiple Mad Libs created in Swift. These Mad Libs mini-games demonstrate the following:

* Printing text to the terminal/console
* Storing information into variables
* Using comments to document code

The Mad Libs files are licensed under the Apache License 2.0. You can look at all of the terms in the [LICENSE](LICENSE) file.

# Running
To run these any Mad Libs file from this repository, you'll need to make use of the Swift binaries for the terminal. You'll need to clone the repository and running the files by doing the following:

```
git clone https://www.gitlab.com/damienmsdevs/swift-mlibs
cd swift-mlibs
swift <madlibfilename>.swift
```
If you have already cloned the repository, you can simply type `git pull` to get the latest files and changes.

# Committing
To push files into the repository, do the following:
```
git add *
git commit -m "<message under 50 chars>"
git push
```
