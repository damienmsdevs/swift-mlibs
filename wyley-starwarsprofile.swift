/* Words to Fill in Mad Lib */
//Let's create the nouns
var firstNoun: String
var secondNoun: String
var thirdNoun: String

//And the adjectives
var firstPastTenseAdjective: String
var secondAdjective: String

//And the verbs
var firstVerb: String
var secondVerb: String
var thirdVerb: String
var fourthVerb: String

//And the adverb
var firstAdverb: String

//Finally, I'll create some extra variables to spice up the story
var firstName: String
var firstLastName: String
var secondName: String

var firstNumber: String
var secondNumber: String

var firstRelative: String

/* Mad Lib Questionnaire */
print("Please enter a noun", terminator: ": ")
firstNoun = readLine()!

print("Please enter another noun", terminator: ": ")
secondNoun = readLine()!

print("Please enter one final noun", terminator: ": ")
thirdNoun = readLine()!

print("Please enter an adjective", terminator: ": ")
firstPastTenseAdjective = readLine()!

print("Please enter another adjective", terminator: ": ")
secondAdjective = readLine()!

print("Please enter a verb", terminator: ": ")
firstVerb = readLine()!

print("Please enter another verb", terminator: ": ")
secondVerb = readLine()!

print("Please enter another verb", terminator: ": ")
thirdVerb = readLine()!

print("Please enter one final verb (this is the last verb, we promise)", terminator: ": ")
fourthVerb = readLine()!

print("Please enter an adverb", terminator: ": ")
firstAdverb = readLine()!

print("Please enter a name", terminator: ": ")
firstName = readLine()!

print("Please enter a name", terminator: ": ")
firstLastName = readLine()!

print("Please enter another name", terminator: ": ")
secondName = readLine()!

print("Please enter a number", terminator: ": ")
firstNumber = readLine()!

print("Please enter another number", terminator: ": ")
secondNumber = readLine()!

print("Please enter a relative's name", terminator: ": ")
firstRelative = readLine()!

/* Mad Lib Story */
//Before we continue, we'll clear the screen for the player so they don't see anything else.
print("\u{001B}[2J")
print("Greetings imbeciles. I am Darth \(firstLastName), the most \(firstPastTenseAdjective) \(firstNoun) in the galaxy.\n")
print("I have destroyed \(firstNumber) planets using a \(secondNoun)-sized weapon called the Death \(thirdNoun). My reputation for killing \(firstAdverb) is something others \(firstVerb) at when they hear my name.'\n")
print("I hate seeing my army \(secondVerb), and the only thing that's worse is when they \(thirdVerb). I have a \(secondAdjective) son that defies me; his name is \(secondName), and he is \(secondNumber) years old.\n")
print("I hardly \(fourthVerb), except when someone decides to get in my way.\n")
print("By the way, I am your \(firstRelative)!")
